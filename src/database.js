var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('We are connected to MongoDB via Mongoose!');
});

// We first define the schema
var Schema = mongoose.Schema;
var blogSchema = new Schema({
  title:  String,
});

// Then we create a model - a model correlates to a collection in MongoDB
var blog = mongoose.model('Blog', blogSchema);

// We create an instance of that model and this would be a document that would be in that collection as defined
var dog = new blog({ title: 'dog' });

// Then we save it to the DB
dog.save()
   .then(doc => {
     console.log('Saving document to collection.')
     console.log(doc)
   })
   .catch(err => {
     console.error(err)
   })

   blog
   .find({
     title: 'dog'   // search query
   })
   .then(doc => {
     console.log('Finding all records with title of dog for blog model:')
     console.log(doc)
   })
   .catch(err => {
     console.error(err)
   })